function ApiPost(n, t, i, r) {
    r == null && (r = !0);
    r && $(".wait_icon").show();
    axios({
        method: "post",
        url: n,
        data: t,
        responseType: "json"
    }, {
        headers: {
            __RequestVerificationToken: $("#hdnAntiForgeryTokens").val()
        }
    }).then(function(n) {
        if (n.data.Code > 0 || n.data.Code == -1) {
            popMessage(n.data.Message, 1);
            n.data.Code == 6 && $("#Msgclose").bind("click", function() {
                window.location.href = "/"
            });
            $(".page-loader").hide();
            return
        }
        i(n);
        $(".page-loader").hide()
    }).catch(function(n) {
        console.log(n);
        $(".page-loader").hide()
    })
}

function base64encode(n) {
    var t, i, f, r, u, e;
    for (f = n.length, i = 0, t = ""; i < f;) {
        if (r = n.charCodeAt(i++) & 255, i == f) {
            t += base64EncodeChars.charAt(r >> 2);
            t += base64EncodeChars.charAt((r & 3) << 4);
            t += "==";
            break
        }
        if (u = n.charCodeAt(i++), i == f) {
            t += base64EncodeChars.charAt(r >> 2);
            t += base64EncodeChars.charAt((r & 3) << 4 | (u & 240) >> 4);
            t += base64EncodeChars.charAt((u & 15) << 2);
            t += "=";
            break
        }
        e = n.charCodeAt(i++);
        t += base64EncodeChars.charAt(r >> 2);
        t += base64EncodeChars.charAt((r & 3) << 4 | (u & 240) >> 4);
        t += base64EncodeChars.charAt((u & 15) << 2 | (e & 192) >> 6);
        t += base64EncodeChars.charAt(e & 63)
    }
    return t
}
var culture = "/en-EN", language;
//axios.defaults.headers.common.__RequestVerificationToken = $("#hdnAntiForgeryTokens").val();
var AjaxPost = function(n, t, i, r, u) {
        u == null && (u = !0);
        u && $(".page-loader").show();
        var e = $("#hdnAntiForgeryTokens").val(),
            f = {};
        f.__RequestVerificationToken = e;
        $.ajax({
            url: n,
            type: "POST",
            async: t,
            dataType: "json",
            headers: f,
            data: i,
            success: function(n) {
                if (n.code > 0) {
                    popMessage(n.msg, 0);
                    return
                }
                r(n);
                $(".page-loader").hide()
            },
            error: function() {
                $(".page-loader").hide()
            },
            cache: !1
        })
    },
    GetMessage = function(n) {
        var t, i = {
            code: n
        };
        return $.ajax({
            url: culture + "/Home/GetMessage",
            type: "POST",
            async: !1,
            data: i,
            success: function(n) {
                t = n
            },
            error: function() {},
            cache: !1
        }), t
    },
    GetResources = function(n) {
        var t, i = {
            code: n
        };
        return $.ajax({
            url: culture + "/Home/GetResources",
            type: "POST",
            async: !1,
            data: i,
            success: function(n) {
                t = n
            },
            error: function() {},
            cache: !1
        }), t
    },
    isNullOrWhitespace = function(n) {
        return typeof n == "undefined" || n == null ? !0 : n.replace(/\s/g, "").length < 1
    },
    changeValidationStatus = function(n, t, i) {
        t ? (n.removeClass("validation-error"), n.addClass("validation-pass"), n.find(".input-field i").removeClass("icon-cancel").addClass("icon-check_circle"), showMessage(n, i), n.find('[data-area="message"]').text("")) : (n.removeClass("validation-pass"), n.addClass("validation-error"), n.find(".input-field i").removeClass("icon-check_circle").addClass("icon-cancel"), showMessage(n, i))
    },
    showMessage = function(n, t) {
        n.find('[data-area="message"]').text(t)
    },
    GetBalance = function(n, t) {
        var i = {
            providerId: n
        };
        ApiPost("/api/Account/GetBalance", i, t)
    },
    GetBankInfoByPaymentMethod = function(n, t, i) {
        var r;
        n == "" ? (r = {
            methodId: t
        }, AjaxPost(culture + "/Account/GetBankInfoByPaymentMethod", !0, r, i)) : (r = {
            bankId: n,
            methodId: t
        }, AjaxPost(culture + "/Account/GetBankInfo", !0, r, i))
    },
    switchPwdVisible = function(n, t) {
        var i = $(t),
            r = $(n).val();
        $(n).attr("type") === "password" ? ($(n).attr("type", "text"), $(t).is("button") ? $(t).text(GetMessage("Hide")) : (i.removeClass("icon-visibility"), i.addClass("icon-visibility_off"))) : ($(n).attr("type", "password"), $(t).is("button") ? $(t).text(GetMessage("Show")) : (i.addClass("icon-visibility"), i.removeClass("icon-visibility_off")));
        $(n).val = r
    },
    /*checkLogin = function() {
        var n, i = $("#hdnAntiForgeryTokens").val(),
            t = {};
        return t.__RequestVerificationToken = i, $.ajax({
            url: culture + "/Account/CheckLogin",
            type: "POST",
            async: !1,
            headers: t,
            dataType: "json",
            data: null,
            success: function(t) {
                n = t
            },
            error: function() {},
            cache: !1
        }), n
    },*/
    popConfirm = function(n, t) {
        $.messagepopup.confirm(n, function(n) {
            if (n == !0) t(!0);
            else {
                t(!1);
                return
            }
        })
    },
    popMessage = function(n, t) {
        if (typeof t == "undefined" || t == null) $.messagepopup.info(n, {
            type: t
        });
        else switch (t) {
            case 0:
                $.messagepopup.success(n, {
                    type: t
                });
                break;
            case 1:
                $.messagepopup.error(n, {
                    type: t
                });
                break;
            default:
                $.messagepopup.info(n, {
                    type: t
                })
        }
    },
    memoryLastUrl = function() {
        var n = document.location.href,
            t = document.location.search,
            i = "";
        return n.indexOf(t) >= 0 && (i = n.replace(t, "")), i.replace("#", "")
    },
    RemoveComma = function(n) {
        return n.toString().replace(/[,]+/g, "")
    },
    AppendComma = function(n, t) {
        var u, i, r, f;
        if (n == 0 || n == "0") return 0;
        if (t) {
            if (u = /^\d+([\.]\d{0,2})?$/.test(n), u && !Number(n)) return n.slice(0, -1)
        } else if (/^((\d+)|(0))$/.test(n))
            if (i = /^((\d+)|(0))$/.exec(n), i != null)
                if (parseInt(i, 10)) n = parseInt(i, 10).toString();
                else return n.slice(0, -1);
        else return n.slice(0, -1);
        else return n.toString().slice(0, -1);
        return n += "", r = n.split("."), f = /(\d{1,3})(?=(\d{3})+$)/g, r[0].replace(f, "$1,") + (r.length >= 2 ? "." + r[1] : "")
    },
    initPromoteId = function() {
        $("#PromoteId").val("");
        $("#PromoteId").attr("data-promotion-rate", "");
        $("#PromoteId").attr("data-formula", "");
        $("#PromoteId").attr("data-max-amt", "");
        $("#PromoteId").attr("data-min-amt", "");
        $("#PromoteId").attr("data-times", "")
    },
    PreviewBonus = function(amount) {
        var transferAmt = Number(RemoveComma(amount)),
            rate = Number($("#PromoteId").attr("data-promotion-rate")),
            maxAmt = Number($("#PromoteId").attr("data-max-amt")),
            minAmt = Number($("#PromoteId").attr("data-min-amt")),
            times = Number($("#PromoteId").attr("data-times")),
            bonus = transferAmt * rate,
            formula = $("#PromoteId").attr("data-formula"),
            promotionBonus;
        if (formula != "" && formula) {
            formula = formula.toLowerCase();
            var r = rate,
                mi = minAmt,
                mx = maxAmt,
                t = times,
                to = transferAmt,
                wl = transferAmt;
            try {
                bonus = eval(formula)
            } catch (e) {
                bonus = 0
            }
        }
        bonus > maxAmt && (bonus = maxAmt);
        bonus < minAmt && (bonus = 0);
        bonus = bonus.toFixed(2);
        promotionBonus = AppendComma(bonus, !0);
        $("#PreviewBonus").val(promotionBonus)
    },
    getPromotions = function(n) {
        AjaxPost(culture + "/Account/GetPromotionsByChangTransferTo", !1, {
            transferto: n
        }, function(n) {
            n.Code == 0 && n.List.length > 0 ? ($("#Promotionli").empty(), $("#Promotionli").append(' <option id="NoPromotion" value="">' + GetMessage("NoThanks") + "<\/option>"), $.each(n.List, function(n, t) {
                var i = "";
                t.IsCustom && (i = t.CustomContent);
                $("#Promotionli").append(' <option data-promotion-rate="' + t.Rate + '" data-max-amt="' + t.MaxAmt + '" data-min-amt="' + t.MinAmt + '" data-times="' + t.Times + '" data-formula="' + i + '" value="' + t.PromoteId + '">' + t.PromoteTitle + "<\/option>")
            }), $("#promotionlistDiv").show(), $("#Promotionli").change(function() {
                var n = $(this).find("option:selected"),
                    t;
                if (n.attr("id") == "NoPromotion") {
                    initPromoteId();
                    $("#PreviewBonus").text("");
                    $("#PreviewBonusBox").hide();
                    return
                }
                $("#PromoteId").val(n.val());
                $("#PromoteId").attr("data-promotion-rate", n.attr("data-promotion-rate"));
                $("#PromoteId").attr("data-formula", n.attr("data-formula"));
                $("#PromoteId").attr("data-max-amt", n.attr("data-max-amt"));
                $("#PromoteId").attr("data-min-amt", n.attr("data-min-amt"));
                $("#PromoteId").attr("data-times", n.attr("data-times"));
                $("#PreviewBonusBox").show();
                t = Number(RemoveComma($("#Amount").val()));
                PreviewBonus(t)
            }), initPromoteId(), $("#PreviewBonus").text(""), $("#PreviewBonusBox").hide()) : ($("#promotionlistDiv").hide(), $("#PromoteId").val(""), initPromoteId(), $("#PreviewBonusBox").hide())
        }, !0)
    };
$.until = {};
$.until.url = function() {};
$.until.url.getUrlParam = function(n) {
        var i = new RegExp("(^|&)" + n + "=([^&]*)(&|$)"),
            t = window.location.search.substr(1).match(i);
        return t != null ? unescape(t[2]) : null
    },
    function() {
        function r() {
            var n = $("#side_nav"),
                i = $("#menu_icon"),
                t = $("#sidenav_overlay"),
                r = $("#icon_close");
            i.on("click", function() {
                n.toggleClass("side-show");
                n.hasClass("side-show") ? t.addClass("sidenav-overlay") : t.removeClass("sidenav-overlay")
            });
            $("#sidenav_overlay, #icon_close").on("click", function(u) {
                u.target !== i.get(0) && u.target !== i.parent().get(0) && n.get(0) !== u.target && ($.contains(n.get(0), u.target) && (u.target !== r.get(0) || $("#dialog").is(":visible")) && (u.target !== r.get(0) || $("#FastTransfer").is(":visible")) || (n.removeClass("side-show"), n.hasClass("side-show") ? t.addClass("sidenav-overlay") : t.removeClass("sidenav-overlay")))
            })
        }

        function u() {
            var n = $("#BottomMenuSelect").val(),
                t = $('#bottom-nav-bar li a[id="' + n + '"]');
            $("#bottom-nav-bar li a").removeClass("active");
            t.toggleClass("active");
            $("#bottom-nav-bar li a").on("click", function() {
                if (this.id === "Financial" && isLogin === "FALSE") return popMessage(GetMessage("LoginPlease")), !1;
                this.id !== "Contact" && $(".page-loader").show()
            })
        }

        function f() {
            $("#LogoutButton").on("click", function(n) {
                n.preventDefault();
                $(".page-loader").hide();
                $("#logoutUrlHidden").val(memoryLastUrl);
                $("#LogoutActionFrm").submit()
            })
        }

        function n() {
            if ($("#pagetop").length) {
                var t = 100,
                    n = function() {
                        var n = $(window).scrollTop();
                        n > t ? $("#pagetop").show() : $("#pagetop").hide()
                    };
                n();
                $(window).on("scroll", function() {
                    n()
                });
                $("#pagetop").on("click", function(n) {
                    n.preventDefault();
                    $("html,body").animate({
                        scrollTop: 0
                    }, 700)
                })
            }
        }

        function e() {
            AjaxPost(i + "/Home/GetAnnouncement", !1, {
                act: "showNotice"
            }, function(n) {
                n.Code > 0 || o(n)
            }, !1)
        }

        function o(n) {
            var r = n.List,
                i;
            if (r.length < 1) return !1;
            i = [];
            $.each(r, function(n, r) {
                switch (t) {
                    case "en-US":
                        r.DispEn && $.trim(r.DispEn) !== "-" && i.push("<li>" + r.DispEn + "<\/li>");
                        break;
                    case "id-ID":
                        r.DispId && $.trim(r.DispId) !== "-" && i.push("<li>" + r.DispId + "<\/li>");
                        break;
                    case "zh-CN":
                        r.DispCn && $.trim(r.DispCn) !== "-" && i.push("<li>" + r.DispCn + "<\/li>");
                        break;
                    case "th-TH":
                        r.DispTh && $.trim(r.DispTh) !== "-" && i.push("<li>" + r.DispTh + "<\/li>");
                        break;
                    case "vi-VN":
                        r.DispVn && $.trim(r.DispVn) !== "-" && i.push("<li>" + r.DispVn + "<\/li>");
                        break;
                    case "en-MY":
                        r.DispMy && $.trim(r.DispMy) !== "-" && i.push("<li>" + r.DispMy + "<\/li>");
                        break;
                    case "ko-KR":
                        r.DispKr && $.trim(r.DispKr) !== "-" && i.push("<li>" + r.DispKr + "<\/li>");
                        break;
                    default:
                        r.DispEn && $.trim(r.DispEn) !== "-" && i.push("<li>" + r.DispEn + "<\/li>")
                }
            });
            r.length > 0 && ($("#marquee").html(i.join("")), $("#announcementContent").html(i.join("")), $("#marquee").marquee({
                scrollSpeed: 24
            }))
        }

        function s() {
            $("#newsInfo").on("click", function() {
                n = c();
                $("body").addClass("dialog-open");
                $("body").css("top", -n + "px");
                $("#announcement").show()
            });
            $("#announcement .icon-close, #announcement .btn").on("click", function() {
                $("body").removeClass("dialog-open");
                h(n);
                $("#announcement").hide()
            })
        }

        function h(n) {
            $("body").scrollTop = $(document).scrollTop = n
        }

        function c() {
            return $("body").scrollTop || $(document).scrollTop
        }
        var t = "en-EN",
            i = "/" + "en-EN";
        isLogin = "FALSE";
        isLogin === "TRUE" && $.contains($("body").get(0), $("#marquee").get(0)) && (e(), s());
        r();
        //u();
        f();
        n();
//        $("#CurrLanguage").on("change", function() {
//            var n = this.value,
//                i, r;
//            $.cookie("language", n, {
//                path: "/"
//            });
//            i = window.location.pathname;
//            r = i.replace(t, n);
//            window.location.replace(r)
//        });
        n = 0;
        $(document).on("click", ".popup-defaul button", function() {
            $("#dialog").remove()
        })
    }();
language = "en-EN";
culture = "/" + "en-EN";
$(function() {
        function i(n) {
            var f = !0,
                e = $(n).attr("href"),
                i = window.location.origin + e,
                u = (RegExp("Provider=(.+?)(&|$)").exec(e) || [, null])[1];
            return u === "ROYAL" && (f = r()), f ? "CMD,IBC,".indexOf(u) > -1 ? isLogin === "FALSE" ? (window.open(i, "_blank"), !0) : t(u, i, !1, !1) : isLogin === "TRUE" ? i.toLocaleLowerCase().indexOf("realmoney=false") > -1 ? (window.open(i, "_blank"), !0) : $(n).parents("ul").is("#submenu_Poker") || u === "TX" ? t(u, i, !0, !0) : t(u, i, !1, !0) : (popMessage("<a href='" + culture + "/Home'>" + GetMessage("LoginPlease") + "<\/a>"), !1) : (popMessage(GetResources("Code_DeviceNotSupport"), 1), !1)
        }

        function r() {
            return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
        }

        function t(n, t, i, r) {
            var f = {
                    ProviderId: n,
                    CheckPlayPoker: i,
                    CheckDeposit: r
                },
                u = !1;
            axios({
                method: "post",
                url: "/api/Member/CheckFastTransfer",
                data: f,
                responseType: "json"
            }, {
                headers: {
                    __RequestVerificationToken: $("#hdnAntiForgeryTokens").val()
                }
            }).then(function(n) {
                var i = n.data,
                    r, f;
                if (i.Code > 0 || i.Code == -1) return popMessage(i.Message), i.Code == 6 && $("#Msgclose").bind("click", function() {
                    window.location.href = "/"
                }), !1;
                if (i.Code == 0) {
                    if (!i.Data.CheckPlayPoker || !i.Data.CheckDeposit) return popMessage(i.Message), !1;
                    i.Data.Approve ? ($("#FastTransfer").show(), $("#TransferFrom").val("-"), $("#LastTransferFrom").val("-"), $("#TransferTo").val(i.Data.ProviderId), $("#LastTransferTo").val(i.Data.ProviderId), $("#TransferToName").val(i.Data.ProviderName), $("#FromCash").html(parseFloat(i.Data.MainwalletAmt).toFixed(2)), $("#ToCash").html(parseFloat(i.Data.ProviderAmt).toFixed(2)), r = $.cookie("the_last_transfer_amt") ? $.cookie("the_last_transfer_amt") : 0, f = parseFloat(r) !== 0 && parseFloat(i.Data.MainwalletAmt) >= parseFloat(r) ? r : i.Data.MainwalletAmt, $("#Amount").val(AppendComma(Math.floor(parseFloat(f))), !1), $("#GameUrl").val(t), getPromotions($("#TransferTo").val()), u = !1) : u = !0
                }
            }).catch(function(n) {
                console.log(n);
                u = !1
            }).then(function() {
                u && window.open(t, "_blank")
            })
        }

        function n(n, t) {
            var i = RemoveComma($.trim($(n).val()));
            $(n).val(AppendComma(i, t))
        }
        $("#returnlastpage").on("click", function(n) {
            n.preventDefault();
            window.history.back()
        });
        /*$("#querymainwallet").on("click", function() {
            var n = $(this).parent().siblings().html().split("&nbsp;")[0];
            GetBalance("-", function(t) {
                $("#mainamt").html(n + "&nbsp;" + parseFloat(t.data.Data.Balance).toFixed(2))
            })
        });*/
        $("a[data-role='member']").on("click", function() {
            isLogin !== "FALSE" && AjaxPost(culture + "/Base/CheckSession", !1, null, function(n) {
                n > 0 && alert(GetMessage("SessionTimeout"))
            }, !1)
        });
        /*$(".game-list a, #gameUl a").on("click", function(n) {
            if (n.preventDefault(), $(this).attr("href").indexOf("/GamePage") >= 0) return i(this), !0;
            window.location.href = this.href
        });*/
        var u = function() {
            $(':input[type=text][data-comma="true"]').each(function(t, i) {
                $(i).blur(function() {
                    $(i).attr("data-point") == "true" ? n(this, !0) : n(this, !1)
                }).keyup(function() {
                    $(i).attr("data-point") == "true" ? n(this, !0) : n(this, !1)
                })
            })
        }();
        $(document).on("keyup blur", ':input[type=text][data-promote="true"]', function() {
            $("#PromoteId").val() != "" && PreviewBonus($(this).val())
        });
        $(document).on("click", ':input[type=text][data-select="all"]', function() {
            this.focus();
            this.select();
            /iPhone|iPad|iPod/i.test(navigator.userAgent) && this.setSelectionRange(0, 9999)
        })
    }),
    function(n) {
        function i(i, r, u) {
            var h = r.type,
                f;
            r.type == undefined && (h = "");
            f = n.extend({
                title: h
            }, t.defaults, r);
            f.timeout = typeof f.timeout == "undefined" ? 0 : f.timeout;
            f.showClose = typeof f.showClose == "undefined" | !f.timeout ? !0 : !!f.showClose;
            document.getElementById("dialog") || (dialog = document.createElement("div"), dialog.id = "dialog", r.type == "confirm" ? n(dialog).html('<div class="popup-defaul"><span><i class="icon-help"><\/i><\/span><h3><\/h3><p>' + i + '<\/p> <button class="half-width btn-default" id="MsgConfirm">' + GetMessage("Confirm") + '<\/button><button class="half-width btn-primary" id="MsgCancel">' + GetMessage("Cancel") + "<\/button>") : n(dialog).html('<div class="popup-defaul"><span><i id="MsgType"><\/i><\/span><h3 id="MsgTitle"><\/h3><p id="MsgContent"><\/p><button class="btn-primary" id="Msgclose">' + GetMessage("Close") + "<\/button>"), n(dialog).hide(), document.body.appendChild(dialog), n("#Msgclose, #MsgConfirm, #MsgCancel").click(t.hide), n("#MsgConfirm").click(function() {
                u(!0)
            }), n("#MsgCancel").click(function() {
                u(!1)
            }));
            var c = n("#dialog"),
                e = n("#MsgType"),
                o = n("#MsgTitle"),
                s = n("#MsgContent");
            switch (r.type) {
                case "success":
                    e.addClass("icon-check_circle");
                    o.html("");
                    s.html(i);
                    break;
                case "error":
                    e.addClass("icon-cancel");
                    o.html("");
                    s.html(i);
                    break;
                case "info":
                    e.addClass("icon-info-icon");
                    o.html("");
                    s.html(i);
                    break;
                default:
                    e.addClass("icon-info-icon");
                    o.html("");
                    s.html(i)
            }
            f.timeout && window.setTimeout("$('#dialog').fadeOut(0); $('#sidenav_overlay').removeClass('sidenav-overlay');", f.timeout * 1e3);
            n("body,input:focus").bind("keypress", function(n) {
                n.preventDefault();
                n.which == 13 && t.hide()
            }).addClass("dialog-enter");
            c.fadeIn(0);
            n("#sidenav_overlay").addClass("sidenav-overlay");
            n(document).mouseup(function(i) {
                var r = n("#dialog");
                if (!r.is(i.target) && r.has(i.target).length === 0)
                    if (n("#FastTransfer").is(":visible") === !0) n(".dialog-enter").unbind("keypress"), c.fadeOut(0), n(document).unbind("mouseup");
                    else {
                        const i = t.hide;
                        i();
                        n(document).unbind("mouseup")
                    }
            })
        }
        var t = {};
        t.show = function(n, t) {
            return i(n, t)
        };
        t.error = function(n, t) {
            return typeof t == "undefined" && (t = {}), t.type = "error", i(n, t)
        };
        t.success = function(n, t) {
            return typeof t == "undefined" && (t = {}), t.type = "success", i(n, t)
        };
        t.info = function(n, t) {
            return typeof t == "undefined" && (t = {}), t.type = "info", i(n, t)
        };
        t.confirm = function(n, t, r) {
            return typeof r == "undefined" && (r = {}), r.type = "confirm", i(n, r, t)
        };
        t.hide = function() {
            n(".dialog-enter").unbind("keypress");
            n("#dialog").fadeOut(0);
            n("#sidenav_overlay").removeClass("sidenav-overlay")
        };
        t.defaults = {
            timeout: 0,
            showClose: !0
        };
        n.extend({
            messagepopup: t
        })
    }(jQuery);
var LanguagetoCurrency = JSON.parse('{"en-US":"USD","id-ID":"IDR","zh-CN":"CNY","th-TH":"THB","vi-VN":"VND","en-MY":"MYR","ko-KR":"KRW"}'),
    ProvidertoSupportCurrency = JSON.parse('{"AG":        "IDR,THB,CNY,MYR,VND,USD,KRW","ALLBET":    "IDR,THB,USD,CNY,VND,MYR,KRW","BET188":    "IDR,THB,USD,CNY,VND,MYR","BETSOFT":   "IDR,THB,CNY,VND,MYR,KRW,USD","BGC":       "IDR,THB,CNY,VND,MYR","BOXPOKER":  "IDR","CMD":       "IDR,THB,USD,CNY,VND,MYR,KRW","CQS":       "IDR,THB,USD,CNY,VND,MYR,KRW","DEWAPOKER": "IDR,KRW,VND,THB,MYR,USD","EBET":      "CNY,USD,MYR,IDR,THB,VND","EGAME":     "IDR,THB,USD,CNY,VND,MYR,KRW","GAMESOS":   "IDR,CNY,VND,MYR,THB,KRW,USD","GD":        "IDR,THB,USD,CNY,VND,MYR","GPC":       "IDR,THB,USD,CNY,VND,MYR,KRW","HABANERO":  "IDR,THB,USD,CNY,VND,MYR,KRW","HBS":       "IDR,THB,USD,CNY,VND,MYR,KRW","DGC":       "IDR,THB,USD,CNY,VND,MYR","IBC":       "IDR,THB,USD,CNY,VND,MYR,KRW","ILOTTO":    "CNY","ISIN4D":    "IDR","KENO":      "IDR,THB,USD,CNY,VND,MYR,KRW","MGE":       "IDR,CNY,VND,MYR","MGS":       "IDR,THB,USD,CNY,VND,MYR,KRW","MGC":       "IDR,THB,USD,CNY,VND,MYR,KRW","OG":        "IDR,THB,USD,CNY,VND,MYR,KRW","OK368":     "VND","OPUSCASINO":"IDR,THB,USD,CNY,VND,MYR,KRW","WFT":       "IDR,THB,USD,VND,MYR,KRW","SBOT":      "IDR,THB,USD,CNY,VND,MYR,KRW","OPUSSPORT": "IDR,THB,USD,CNY,VND,MYR","POKER":     "IDR","PT":        "IDR,THB,CNY,VND,MYR,KRW","QQTHAI":    "THB","ROYAL":     "IDR,USD,CNY,VND,MYR","SBO":       "IDR,THB,CNY,MYR","SXC":       "IDR,THB,USD,CNY,VND,MYR,KRW","TTG":       "IDR,THB,USD,CNY,VND,MYR,KRW","TX":        "IDR,THB,USD,CNY,VND,MYR","XOPOKER":   "IDR,THB,USD,CNY,VND,MYR","TGP": \t  "IDR,CNY,MYR,VND,THB,USD","PRAGMATIC": "IDR,THB,USD,CNY,VND,MYR,KRW","PSS":       "IDR,THB,USD,CNY,VND,MYR,KRW","PGS":       "IDR,THB,USD,CNY,VND,KRW"}'),
    base64EncodeChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"